#ifndef _COMMUNICATOR_UC_H_
#define _COMMUNICATOR_UC_H_
#include<stdint.h>
/*
THIS FUNCTION WILL READ DATA FROM THE RX PORT WHICH IS SEND FROM PC
INPUT:
    NONE
OUTPUT:
    uint8_t data,
        this is the data that has been send by pc (for acknowledgement,error,handshake,resend requeset)
    
*/
uint8_t reciver();

/*
THIS FUNCTION WILL SEND THE DATA TO THE PC FROM TX PORT BY MAKING FRAME FOR THE BYTE TO SEND.
INPUT:
    uint8_t data,
        this is the data which we have to send to pc.this data will be as per encoded_packet.
OUTPUT:
    NONE
*/
void send(uint8_t data);

/*
THIS FUNCTION MANAGE THE COMMUNICATION BY HANDLING send AND reciver FUNCTION.
INPUT:
    uint8_t* encoded_packet,
        this pointer will point to the array that will have encoded packet that we have to send to the pc.
OUTPUT:
    NONE
*/
void communication_manager(uint8_t* encoded_packet);

#endif