#ifndef _DATA_ACQUISITION_H_
#define _DATA_ACQUISITION_H_
#include<stdint.h>

/*
THIS FUNCTION READ VALUE FROM THE ADC PORT THAT HAS BEEN PASSED
INPUT:
	uint8_t adc_port,
		the port number from which we have to read value.
RETURN:
	unsigned short,
		the value that is present at adc_port.
*/
unsigned short adc_data_read(uint8_t adc_port);

/*
THIS FUNCTION WILL CALL "adc_data_read" BY PASSING "adc_port" FOR ALL THE SONAR AND ARRANGE THIS VALUE IN AN ARRAY 
INPUT:
	unsigned short* sonar_value_in_sequence,
		this pointer point to the first value of array that will be updated by the value return by "adc_data_read".
		
	uint8_t* port_no_of_sonar,
		this pointer point to the first value of array that will have port no from where the value has to be read.
		this value in array call adc_data_read by passing each value one by one at particular delay. 
	
	uint8_t no_of_sonar, 
		this variable will have the number of sonar present in sensor array.
*/
void data_acquisition(unsigned short* sonar_value_in_sequence, uint8_t* port_no_of_sonar, uint8_t no_of_sonar);

#endif