#ifndef _ENCODER_H_
#define _ENCODER_H_
#include<stdint.h>

/*
THIS FUNCTION WILL FORM A PACKET OF SONAR DATA TO TRANSMIT BY UART TO PC.

THE PACKET WILL BE ENCODED AS PER THE PROTOCOL [START BYTE][PACKET TYPE][SIZE][DATA][END][CRC].
START BYTE :- $.
PACKET TYPE :- 4.
SIZE :- (no_of_sonar*3)+1 "THIS REPRESENT THE NUMBER OF BYTE DATA WILL HAVE" .
DATA :- no_of_sonar,SONAR VALUE AS PER SEQUENCE OF THE SENSOR NUMBER IN THE SENSOR ARRAY(SONAR DATA WILL BE 3 BYTE : 1ST BYTE IS SONAR NUMBER, 2ND AND 3RD BYTES           IS SONAR DATA CORESSPONDING TO SONAR NUMBER MENTIONED IN 1ST BYTE).
END BYTE :- #.
CRC :- XOR OF ALL THE BYTE IN THE PACKET.

INPUT:
    uint8_t* encoded_packet,
        this pointer will point to the first value of array in which the value of encoded packet will be stored.
    
    unsigned short* sonar_value_in_sequence,
        this pointer will point to the first value of array which contain the sonar value read from adc.

    uint8_t no_of_sonar,
        this variable will have value corresponding to total no of sonar in array.
OUTPUT:
     NONE.
*/
void encode_packet(uint8_t* encoded_packet,unsigned short* sonar_value_in_sequence,uint8_t no_of_sonar);

#endif