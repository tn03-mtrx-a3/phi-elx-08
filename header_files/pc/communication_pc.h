#ifndef _COMMUNICATOR_PC_H_
#define _COMMUNICATOR_PC_H_
#include<stdint.h>

/*
THIS FUNCTION WILL READ DATA FROM THE RX PORT WHICH IS SEND FROM uC
INPUT:
    NONE
OUTPUT:
    uint8_t ,
        this is the data that has been send by uc (for handshake,data from packet)
    
*/
uint8_t reciver();

/*
THIS FUNCTION WILL SEND THE DATA TO THE uC FROM TX PORT BY MAKING FRAME FOR THE BYTE TO SEND.
INPUT:
    uint8_t data,
        this is the data which we have to send to uc.this data will be as per the request made by pc to uc(ackonwledgement,handsake,error,resend,completion).
OUTPUT:
    NONE
*/
void send(uint8_t data);

/*
THIS FUNCTION MANAGE THE COMMUNICATION BY HANDLING send AND reciver FUNCTION.
INPUT:
    uint8_t* recived_packet,
        this pointer will point to the array that will be updated as per recived encoded packet that we have recived from the uc.
OUTPUT:
    NONE
*/
void communication_manager(uint8_t* recived_packet);

#endif