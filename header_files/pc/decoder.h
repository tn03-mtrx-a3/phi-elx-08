#ifndef _DECODER_H_
#define _DECODER_H_
#include <stdint.h>

/*
THIS FUNCTION WILL decoded the packet recived from uc as per the protocol and update the another array which will contain adc data.
INPUT:
    uint8_t* recived_packet,
        this pointer will point to the first value of array in which the value of encoded recived packet will be stored.
    
    unsigned short* sonar_value_in_sequence,
        this pointer will point to the first value of array which will update as per sonar value read from adc present in the recived encoded packet from uc.

    uint8_t no_of_sonar,
        this variable will have value corresponding to total no of sonar in array.
*/
void decode_packet(uint8_t* recived_packet, unsigned short *sonar_value_from_adc);

#endif